package com.exercise.deploypractice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/get")
public class GetController {
    @GetMapping("/{value}")
    public ResponseEntity<String> print(@PathVariable String value){
        return ResponseEntity.ok().body("input string: " + value);
    }
}
